function getMatchedMenuItem(menu, idOrSlugOrPath) {
    let targetItem;

    if (Object.prototype.toString.call(menu) === '[object Array]') {
        menu.forEach(function (menuItem) {
            if (menuItem.id && menuItem.id.toString() === idOrSlugOrPath.toString()) {
                targetItem = menuItem;
            } else if (menuItem.path && menuItem.path.toString() === idOrSlugOrPath.toString()) {
                targetItem = menuItem;
            } else if (menuItem.slug && menuItem.slug.toString() === idOrSlugOrPath.toString()) {
                targetItem = menuItem;
            } else if (menuItem.children && !targetItem) {
                targetItem = getMatchedMenuItem(menuItem.children, idOrSlugOrPath);
            }

            return !targetItem;
        });
    }

    return targetItem;
}

const defaults = {
    env: 'dev',
    corePath: '',
    mediaPath: '',
    vendorPath: '',
    bem: '_'
};

module.exports = {
    filters: function (globals) {
        globals = Object.assign({}, defaults, globals || {});

        function typografy(string) {
            const regExp = new RegExp('(^|\\s|[^a-zа-яё<.,_-])([a-zа-яё0-9]{1,3})\\s', 'gi');
            return string.toString()
                .replace(/[\r\n]+/g, ' ')
                .replace(/[ ]+/g, ' ')
                .replace(/^[—]\s/g, '&mdash;&nbsp;')
                .replace(/\s[-]\s/g, '&nbsp;&mdash;&thinsp;')
                .replace(/\s[—]\s/g, '&nbsp;&mdash;&thinsp;')
                .replace(/[—]/g, '&mdash;')
                .replace(/([0-9])[xх]([0-9])/ig, '$1&times;$2')
                .replace(/([^=]|^)"([^"=<>]+)"/ig, '$1&laquo;$2&raquo;')
                .replace(regExp, '$1$2&nbsp;').replace(regExp, '$1$2&nbsp;');
        }

        function slugify(string) {
            const ru = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'];
            const en = ['a', 'b', 'v', 'g', 'd', 'e', 'yo', 'zh', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sh', 'u', 'y', '', 'e', 'yu', 'ya'];
            const slug = string.toString()
                .trim()
                .toLowerCase()
                .replace(/[^a-zа-яё0-9]/g, '-')
                .replace(/[-]+/g, '-')
                .replace(/^[-]+/g, '')
                .replace(/[-]+$/g, '')
                .split('-')
                .slice(0, 6)
                .join('-');

            return slug.split('').map(function (char) {
                const ruIndex = ru.indexOf(char);
                return ruIndex !== -1 ? en[ruIndex] : char;
            }).join('');
        }

        function digitify(number, params) {
            const decimals = params && params[0];
            const dec_point = params && params[1];
            const thousands_sep = params && params[2];
            if (parseFloat(number).toString() !== number && isNaN(number) || number.toString().length <= 3) {
                return number;
            }

            const stringNumber = decimals ? parseFloat(number).toFixed(decimals) : number.toString();
            const partsNumber = stringNumber.split('.');
            const ceilNumber = partsNumber[0];
            const decNumber = partsNumber[1];
            return ceilNumber.split('').reverse()
                .join('').split(/([0-9]{3})/)
                .filter(function (i) {
                    return i;
                }).join(thousands_sep || ' ')
                .split('').reverse().join('') + (decNumber ? (dec_point || ',') + decNumber : '');
        }

        function phonify(number) {
            const numberParts = number.toString().split(/[^0-9]+доб[^0-9]+/i);
            const phone = numberParts[0];
            const extNumber = numberParts[1] && numberParts[1].replace(/[^0-9+]/g, '');
            return phone ? phone
                .replace(/^[8]/, '+7')
                .replace(/[^0-9+]/g, '') + (extNumber ? ';ext=' + extNumber : '') : phone;
        }

        function urlify(menu, params) {
            const [id, hash] = params && params[0] ? params[0].split('#') : ['', ''];
            const fallbackURL = params && params[1];
            const matchedItem = getMatchedMenuItem(menu, id);
            return matchedItem ? matchedItem.url + (hash ? '#' + hash : '') : (fallbackURL || id + (hash ? '#' + hash : ''));
        }

        function matchify(menu, params) {
            const matched = [];
            const key = params && params[0] || undefined;
            const value = params && params[1] || undefined;

            if (Object.prototype.toString.call(menu) === '[object Array]' && menu.length) {
                menu.forEach(function (menuItem) {
                    if (menuItem[key] !== undefined && menuItem[key] === value) {
                        matched.push(menuItem);
                    }
                });
            }

            return matched;
        }

        function fileSizify(bytes) {
            const thresh = 1000;
            if (Math.abs(bytes) < thresh) {
                return bytes + 'б';
            }
            const units = ['кб', 'Мб', 'Гб', 'Тб', 'Пб'];
            let u = -1;
            do {
                bytes /= thresh;
                ++u;
            } while (Math.abs(bytes) >= thresh && u < units.length - 1);
            return (parseFloat(bytes.toFixed(1))).toString().replace('.', ',') + units[u];
        }

        function hashify(value, params) {
            const magic = 2378757;
            let int = value;
            let length = params && params[0] || undefined;
            length = length ? length > 12 ? 12 : length : 6;
            if (typeof value === 'object' || typeof value === 'string' && !value.length) {
                value = JSON.stringify(value);
            }
            if (typeof value === 'function') {
                value = value.toString();
            }
            if (typeof value === 'string') {
                int = 0;
                value.split('').forEach(function (currentValue, index) {
                    const charCode = currentValue.charCodeAt(0);
                    int += (Math.abs(charCode) + index) / (magic + charCode);
                });
            } else if (typeof value === 'undefined' || typeof value === 'undefined') {
                int = Math.random();
            }
            return ((Math.abs(int) + 1) / (magic + int)).toString(16).substr(length * -1);
        }

        function postfixify(image, params) {
            const postfix = params && params[0] || undefined;
            return image.replace(/\.([a-z]+)$/i, `${postfix || '@2x'}.$1`);
        }

        function webpify(image) {
            return image.replace(/\.([a-z]+)$/i, '.webp');
        }

        function modify(base, params) {
            let modificators = params && params[0] || '';
            if (!params[0] && !params[2]) {
                modificators = 'default';
            }
            const isJs = base.indexOf('js-') === 0;
            const connector = params[1] || (isJs ? '-' : globals.bem);
            const classArray = modificators.toString().split(/[ ,]+/).map(function (modificator) {
                return modificator ? base + connector + modificator : '';
            });

            if (!isJs || params[2]) {
                classArray.unshift(base);
            }

            return classArray.join(' ').trim();
        }

        function cyclefy(index, params) {
            return params[(index - 1) % params.length];
        }

        function extendify(target, params) {
            const defaults = params[0];
            const rewriteTarget = params[1];
            let result;

            if (Array.isArray(target)) {
                result = target.map((item) => {
                    return extendify(item, params);
                });
            } else {
                result = {};
                if (rewriteTarget) {
                    Object.assign(result, target, defaults);
                } else {
                    Object.assign(result, defaults, target);
                }
            }

            return result;
        }

        function extrafy(target, params) {
            return target && typeof target === 'object' ? Object.keys(target)
                .sort().filter(function (key) {
                    return key !== '_keys';
                }).reduce(function (result, key, index, array) {
                    if (target[key] !== undefined && target[key] !== false) {
                        if (target[key] === true) {
                            result.push(key);
                        } else {
                            result.push(key + '="' + target[key] + '"');
                        }
                    }

                    return result;
                }, []).join(' ') : '';
        }

        function testify(target, params) {
            return target && !!target.match(new RegExp(params[0], params[1]));
        }

        function inGroup(menuItem, params) {
            const group = params && params[0] || undefined;

            if (!group) {
                return true;
            }

            if (group && !menuItem.group) {
                return false;
            }

            if (menuItem.group && menuItem.group === group) {
                return true;
            }

            return menuItem.group.split(/\s*,\s*/).indexOf(group) !== -1;
        }

        function hasMenu(self) {
            let hasItems = false;
            const level = self.level || 1;

            if (self.menu && self.menu.length && self.maxDepth === -1 || self.maxDepth >= level || self.forceChildren) {
                const menuItems = self.menu.filter(menuItem => {
                    return !menuItem.skip && inGroup(menuItem, [self.group]);
                });

                hasItems = !!menuItems.length;
            }

            return hasItems;
        }

        function jsClassify(self, params) {
            if (self.jsClassName) {
                const className = params && params[0] || undefined;
                if (className) {
                    var jsClasses = self.jsClassName.trim().split(/[ ]+/);
                    jsClasses[0] = Array.prototype.concat(jsClasses[0].split('-').slice(0, 2), className)
                        .filter(el => !!el).join('-');
                    return ' ' + jsClasses.join(' ');
                } else {
                    return ' ' + self.jsClassName;
                }
            } else {
                return '';
            }
        }

        function sortifyBy(self, params) {
            const keyName = params && params[0] || undefined;
            let result = self;

            if (Array.isArray(self) && keyName) {
                result = self.sort((a, b) => {
                    return a[keyName] > b[keyName] ? 1 : (a[keyName] < b[keyName] ? -1 : 0);
                });
            }
            return result;
        }

        return {
            cycle: cyclefy,
            digit: digitify,
            extend: extendify,
            extra: extrafy,
            fileSize: fileSizify,
            ft: typografy,
            hasMenu: hasMenu,
            hash: hashify,
            inGroup: inGroup,
            jsClass: jsClassify,
            matchArray: matchify,
            mod: modify,
            phone2numeric: phonify,
            retina: postfixify,
            slug: slugify,
            sortBy: sortifyBy,
            test: testify,
            url: urlify,
            webp: webpify,
        };
    },

    functions: function (globals) {
        globals = Object.assign({}, defaults, globals || {});

        function getCorePath(url) {
            return (globals.corePath || '').replace(/^[.]/, '') + url;
        }

        function getMediaPath(url) {
            return (globals.mediaPath || '').replace(/^[.]/, '') + url;
        }

        function getVendorPath(url) {
            return (globals.vendorPath || '').replace(/^[.]/, '') + url;
        }

        function getPage(page, menu) {
            if (page.id) {
                const menuItem = getMatchedMenuItem(menu, page.id);
                if (menuItem) {
                    page = Object.assign({}, menuItem, page);
                }
            }
            return page;
        }

        function getParentFor(menu, idOrSlug, maxDepth) {
            const menuItem = getMatchedMenuItem(menu, idOrSlug);
            let parent = null;

            function getParentId(pathArray) {
                return pathArray.slice(-2, -1)[0];
            }

            if (menuItem) {
                const itemPathArray = menuItem.path.split('/');

                if (itemPathArray.length > 1) {
                    let parentId = getParentId(itemPathArray);

                    if (maxDepth) {
                        while (itemPathArray.indexOf(parentId) !== -1 && itemPathArray.indexOf(parentId) > maxDepth - 1) {
                            itemPathArray.pop();
                            parentId = getParentId(itemPathArray);
                        }
                    }

                    parent = getMatchedMenuItem(menu, parentId);
                }
            }
            return parent;
        }

        function getRoot(page, menu, rootLevel) {
            let rootItem;
            if (page.path) {
                rootLevel = rootLevel || 1;
                const pathParts = page.path.split('/');
                const rootId = pathParts[rootLevel - 1] || pathParts[0];

                rootItem = getMatchedMenuItem(menu, rootId);
            }
            return rootItem;
        }

        function getPageTitle(page, menu) {
            if (page.title) {
                return page.title;
            } else if (page.id) {
                const menuItem = getMatchedMenuItem(menu, page.id);
                return menuItem.label;
            } else {
                return '';
            }
        }

        function getMenuItem(menu, id) {
            return getMatchedMenuItem(menu, id);
        }

        function getMenu(menu, id, group) {
            let items;
            const menuItem = getMatchedMenuItem(menu, id);
            if (menuItem) {
                items = menuItem.children;

                if (items && items.length && group) {
                    items = items.filter((item) => {
                        return item.group && item.group.split(/\s*,\s*/).indexOf(group) !== -1;
                    });
                }
            }
            return items;
        }

        function isActive(menuItem, page) {
            const pagePathArray = page ? page.path.split('/') : [''];
            return menuItem.id ? pagePathArray.indexOf(menuItem.id.toString()) !== -1 : false;
        }

        function isCurrent(menuItem, page) {
            const pagePathArray = page ? page.path.split('/') : [''];
            return menuItem.id ? menuItem.id.toString() === pagePathArray.pop().toString() : false;
        }

        return {
            core: getCorePath,
            vendor: getVendorPath,
            media: getMediaPath,
            getMenu: getMenu,
            getMenuItem: getMenuItem,
            getPageTitle: getPageTitle,
            getRoot: getRoot,
            getPage: getPage,
            getParentFor: getParentFor,
            isActive: isActive,
            isCurrent: isCurrent,
        };
    },

    extensions: function (globals) {
        globals = Object.assign({}, defaults, globals || {});

        return [];
    }
};
