# @toptalo/twig-helpers [![pipeline status](https://gitlab.com/toptalo/twig-helpers/badges/master/pipeline.svg)](https://gitlab.com/toptalo/twig-helpers/commits/master)


Кастомные функции и фильтры


## Установка

```shell
npm install @toptalo/twig-helpers --save
```

## Есть функции:

* `{{ core('css/global.css') }}` - возвращает путь для `/static/core/css/global.css` 
* `{{ vendor('jquery.min.js') }}` - возвращает путь для `/static/vendor/jquery.min.js` 
* `{{ media('image.jpg') }}` - возвращает путь для `/static/core/markup-media/image.jpg`
* `{{ getMenu(menu, id|path|slug, ?group) }}` - возвращает массив `childrens` объекта меню с заданным `id`, `path` или `slug`
* `{{ getMenuItem(menu, id|path|slug) }}` - возвращает объект меню с заданным `id`, `path` или `slug`
* `{{ getPageTitle(page, menu) }}` - возвращает заголовок страницы из меню, сопоставляя по `page.id`
* `{{ getRoot(page, menu, ?rootLevel) }}` - возвращает корневой элемент меню для переданной страницы, отступ от корня регулируется параметром `rootLevel` - по умолчанию 1
* `{{ getPage(page, menu) }}` - возврашает объект старницы обогащённый данными из меню
* `{{ getParentFor(menu, id|path|slug) }}` - возвращает родительский элемент меню
* `{{ isActive(menuItem, page) }}` - возвращает `true` если `menuItem.id` есть в пути `page.path`
* `{{ isCurrent(menuItem, page) }}` - возвращает `true` если `menuItem.id` последний в пути `page.path` 

## Есть фильтры:

* `{{ 'text'|slug }}` - возвращает слаг для строки `text` (нормализованную строку) 
* `{{ 'text'|ft }}` - типографика (длинное тире, пробелы) 
* `{{ '11590'|digit(?decimals, ?dec_point, ?thousands_sep) }}` - возвращает число разбитое по разрядам `11 590`
* `{{ '+7 (999) 888-77-66'|phone2numeric }}` - возвращает чистый номер `+79998887766` 
* `{{ menu|url(id|path|slug) }}` - возвращает `url` из пункта меню `menu` с заданным `id`, `path` или `slug` 
* `{{ menu|matchArray(key, value) }}` - вернет пункты меню у которых `key` === `value` 
* `{{ '1024'|fileSize }}` - вернет `1кб` для 1 000 байт, `1Мб` для 1 000 000 байт
* `{{ loop.index|hash }}` - делает хеш из числа, строки, объекта, или функции (удобно для айдишников)
* `{{ loop.index|cycle('one', 'two', 'three') }}` - используя индекс повторяет строки из переданного массива
* `{{ target|extend(defaults, rewriteTarget) }}` - расширяет объект `defaults` объектом `target` или наоборот если передан флаг `rewriteTarget`. `defaults` может быть массивом объектов, тогда действие будет применено ко всем элементам массива
* `{{ 'block'|mod(?modificators, ?connector, ?skipDefault{boolean}) }}` - добавляет модификаторов на класс, используя `connector(_)`, если модификаторы не переданы (строка, разделённая запятыми), добавляет модификатор `default`, можно отключить передав третий параметр `skipDefault` равный `true`  
* `{{ 'image.jpg'|retina(?postfix) }}` - добавляет постфикс в имя файла, по умолчанию `@2x` (image@2x.jpg)
* `{{ 'image.jpg'|webp }}` - заменяет расширение в пути на `webp`
* `{{ {key: value}|extra }}` - разворачивает объект в HTML аттрибуты
* `{{ 'string'|test(re, ?flags) }}` - проверяет по регулярке на совпадение
* `{{ menuItem|inGroup(groupName) }}` - проверяет вхождение элемента меню в указанную группу `groupName`. Группа меню указывается у элемента в параметре `group: 'groupName'`. Элемент меню может входить в несколько групп `group: 'groupName1, groupName2'`.
* `{{ self|hasMenu }}` - проверяет есть ли пункты меню для вывода, учитывая стандартные фильтры.
* `{{ self|jsClass(className) }}` - добавляет `className` к `self.jsClassName`
* `{{ self|sortBy(keyName) }}` - сортирует список объектов `self` по ключу `keyName`

## При поддержке

[![DesignDepot](https://designdepot.ru/static/core/img/logo.png)](https://designdepot.ru/?utm_source=web&utm_medium=npm&utm_campaign=twig-helpers)

## История релизов

Смотри [CHANGELOG](CHANGELOG.md).
