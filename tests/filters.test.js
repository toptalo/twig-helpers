const twigHelpers = require('../twig-helpers');

const context = require('./context.json');

const filters = twigHelpers.filters(context.params);

describe('Twig Фильтры:', () => {
    describe('Фильтр `cycle`', () => {
        test('Должен вернуть `undefined` для значения `-1`', () => {
            expect(filters.cycle('-1', ['one', 'two', 'three']))
                .toBe(undefined);
        });
        test('Должен вернуть `undefined` для значения `0`', () => {
            expect(filters.cycle('0', ['one', 'two', 'three']))
                .toBe(undefined);
        });
        test('Должен вернуть `"one"` для значения `1`', () => {
            expect(filters.cycle('1', ['one', 'two', 'three']))
                .toBe('one');
        });
        test('Должен вернуть `"two"` для значения `2`', () => {
            expect(filters.cycle('2', ['one', 'two', 'three']))
                .toBe('two');
        });
        test('Должен вернуть `"three"` для значения `3`', () => {
            expect(filters.cycle('3', ['one', 'two', 'three']))
                .toBe('three');
        });
        test('Должен вернуть `"one"` для значения `4`', () => {
            expect(filters.cycle('4', ['one', 'two', 'three']))
                .toBe('one');
        });
    });

    describe('Фильтр `digit`', () => {
        test('Должен вернуть `"1 000,5"` для значения `1000.5`, если вызван без параметров', () => {
            expect(filters.digit('1000.5'))
                .toBe('1 000,5');
        });
        test('Должен вернуть `"1 000,50"` для значения `1000.5`, если decimals=2', () => {
            expect(filters.digit('1000.5', ['2']))
                .toBe('1 000,50');
        });
        test('Должен вернуть `"1 000.50"` для значения `1000.5`, если decimals=2 и dec_point="."', () => {
            expect(filters.digit('1000.5', ['2', '.']))
                .toBe('1 000.50');
        });
        test('Должен вернуть `"1,000.50"` для значения `1000.5`, если decimals=2, dec_point="." и thousands_sep=","', () => {
            expect(filters.digit('1000.5', ['2', '.', ',']))
                .toBe('1,000.50');
        });
    });

    describe('Фильтр `extend`', () => {
        test('Должен расширить переданный объект, перезаписывая значения', () => {
            expect(filters.extend({a: 1, b: 2}, [{b: 1, c: 3}]))
                .toMatchObject({a: 1, b: 2, c: 3});
        });
        test('Должен расширить переданный объект, сохраняя значения', () => {
            expect(filters.extend({a: 1, b: 2}, [{b: 1, c: 3}, true]))
                .toMatchObject({a: 1, b: 1, c: 3});
        });
        test('Должен расширить объекты переданные в массиве, перезаписывая значения', () => {
            expect(filters.extend([{a: 1, b: 2}, {a: 2, b: 3}], [{b: 1, c: 3}]))
                .toMatchObject([{a: 1, b: 2, c: 3}, {a: 2, b: 3, c: 3}]);
        });
        test('Должен расширить объекты переданные в массиве, сохраняя значения', () => {
            expect(filters.extend([{a: 1, b: 2}, {a: 2, b: 3}], [{b: 1, c: 3}, true]))
                .toMatchObject([{a: 1, b: 1, c: 3}, {a: 2, b: 1, c: 3}]);
        });
    });

    describe('Фильтр `fileSize`', () => {
        test('Должен конвертировать значение в байты', () => {
            expect(filters.fileSize(1))
                .toBe('1б');
        });
        test('Должен конвертировать значение в килобайты', () => {
            expect(filters.fileSize(1e3))
                .toBe('1кб');
        });
        test('Должен конвертировать значение в мегабайты', () => {
            expect(filters.fileSize(1e6))
                .toBe('1Мб');
        });
        test('Должен конвертировать значение в гигабайты', () => {
            expect(filters.fileSize(1e9))
                .toBe('1Гб');
        });
        test('Должен конвертировать значение в терабайты', () => {
            expect(filters.fileSize(1e12))
                .toBe('1Тб');
        });
        test('Должен конвертировать значение в петабайты', () => {
            expect(filters.fileSize(1e15))
                .toBe('1Пб');
        });
    });

    describe('Фильтр `ft`', () => {
        test('Должен заменить `\\r\\n` на ` `', () => {
            expect(filters.ft('\r\n'))
                .toBe(' ');
        });
        test('Должен заменить `  ` на ` `', () => {
            expect(filters.ft('  '))
                .toBe(' ');
        });
        test('Должен заменить ` - ` на `&nbsp;&mdash;&thinsp;`', () => {
            expect(filters.ft(' - '))
                .toBe('&nbsp;&mdash;&thinsp;');
        });
        test('Должен заменить ` — ` на `&nbsp;&mdash;&thinsp;`', () => {
            expect(filters.ft(' — '))
                .toBe('&nbsp;&mdash;&thinsp;');
        });
        test('Должен заменить `—` на `&mdash;`', () => {
            expect(filters.ft('—'))
                .toBe('&mdash;');
        });
        test('Должен заменить `1x2` на `1&times;2`', () => {
            expect(filters.ft('1x2'))
                .toBe('1&times;2');
        });
        test('Должен заменить `1х2` (кириллица) на `1&times;2`', () => {
            expect(filters.ft('1х2'))
                .toBe('1&times;2');
        });
        test('Должен заменить `"q"` на `"&laquo;q&raquo;"`', () => {
            expect(filters.ft('"q"'))
                .toBe('&laquo;q&raquo;');
        });
        test('Должен правильно заменить кавычки `<a href="#" target="_blank">"Вперед!"</a>`', () => {
            expect(filters.ft('<a href="#" target="_blank">"Вперед!"</a>'))
                .toBe('<a href="#" target="_blank">&laquo;Вперед!&raquo;</a>');
        });
        test('Должен игнорировать аттрибуты<img src="image.jpg" srcset="image@2x.jpg 2x" alt="image">`', () => {
            expect(filters.ft('<img src="image.jpg" srcset="image@2x.jpg 2x" alt="image">'))
                .toBe('<img src="image.jpg" srcset="image@2x.jpg 2x" alt="image">');
        });
        test('Должен добавить неразрывный пробел перед предлогом', () => {
            expect(filters.ft('qwer ra rb ewqr'))
                .toBe('qwer ra&nbsp;rb&nbsp;ewqr');
            expect(filters.ft('qwe ra rb ewqr'))
                .toBe('qwe&nbsp;ra&nbsp;rb&nbsp;ewqr');
        });
    });

    describe('Фильтр `hash`', () => {
        test('Должен вернуть рандомный хеш если вызван без параметров', () => {
            expect(filters.hash())
                .not.toBe(filters.hash());
        });
        test('Должен вернуть одинаковый хеш для одинаковых строк', () => {
            expect(filters.hash('1'))
                .toBe(filters.hash('1'));
        });
        test('Должен вернуть разный хеш для разных строк', () => {
            expect(filters.hash('1'))
                .not.toBe(filters.hash('2'));
        });
        test('Должен вернуть одинаковый хеш для одинаковых массивов', () => {
            expect(filters.hash(['1', '2']))
                .toBe(filters.hash(['1', '2']));
        });
        test('Должен вернуть разный хеш для разных массивов', () => {
            expect(filters.hash(['1', '2']))
                .not.toBe(filters.hash(['3', '4']));
        });
        test('Должен вернуть одинаковый хеш для одинаковых объектов', () => {
            expect(filters.hash({a: 1, b: 2}))
                .toBe(filters.hash({a: 1, b: 2}));
        });
        test('Должен вернуть разный хеш для разных объектов', () => {
            expect(filters.hash({a: 1, b: 2}))
                .not.toBe(filters.hash({a: 2, b: 3}));
        });
        test('Должен вернуть одинаковый хеш для одинаковых функций', () => {
            expect(filters.hash(function a() {
                return 1;
            }))
                .toBe(filters.hash(function a() {
                    return 1;
                }));
        });
        test('Должен вернуть разный хеш для разных функций', () => {
            expect(filters.hash(function a() {
                return 1;
            })).not.toBe(filters.hash(function a() {
                return 2;
            }));
        });
    });

    // describe('Фильтр `hasMenu`', () => {
    // TODO: Добавить тесты
    // });

    describe('Фильтр `matchArray`', () => {
        let target = [{a: 1, b: 1}, {a: 1, b: 2}, {a: 2, b: 3}];
        test('Должен найти объекты с заданными ключом и значением', () => {
            expect(filters.matchArray(target, ['a', 1]))
                .toMatchObject([{a: 1, b: 1}, {a: 1, b: 2}]);
        });
    });

    describe('Фильтр `mod`', () => {
        test('Должен добавить модификатор `default` когда вызван без параметров', () => {
            expect(filters.mod('block', []))
                .toBe('block block_default');
        });
        test('Должен правильно модифицировать js классы', () => {
            expect(filters.mod('js-block', []))
                .toBe('js-block-default');
        });
        test('Должен добавить модификаторы', () => {
            expect(filters.mod('block', [['round', 'white']]))
                .toBe('block block_round block_white');
        });
        test('Должен учесть указанный разделитель модификаторы', () => {
            expect(filters.mod('block', [['round', 'white'], '--']))
                .toBe('block block--round block--white');
        });
        test('Должен пропустить модификатор `default` если передан трети параметр', () => {
            expect(filters.mod('block', [undefined, null, true]))
                .toBe('block');
        });
    });

    describe('Фильтр `phone2numeric`', () => {
        test('Должен удалить лишние символы', () => {
            expect(filters.phone2numeric('+7 (999) 888-77-66'))
                .toBe('+79998887766');
        });
        test('Должен заменить восьмёрку на `+7`', () => {
            expect(filters.phone2numeric('8 (999) 888-77-66'))
                .toBe('+79998887766');
        });
        test('Должен отработать добавочный номер', () => {
            expect(filters.phone2numeric('8 (999) 888-77-66, доб 789'))
                .toBe('+79998887766;ext=789');
        });
    });

    describe('Фильтр `retina`', () => {
        test('Должен добавить `@2x` перед расширением файла', () => {
            expect(filters.retina('image.png'))
                .toBe('image@2x.png');
        });
        test('Должен добавить переданный постфикс перед расширением файла', () => {
            expect(filters.retina('image.png', ['@3x']))
                .toBe('image@3x.png');
        });
    });

    describe('Фильтр `webp`', () => {
        test('Должен заменить расширение файла на `.webp`', () => {
            expect(filters.webp('image.png'))
                .toBe('image.webp');
            expect(filters.webp('image.png.jpg'))
                .toBe('image.png.webp');
        });
    });

    describe('Фильтр `slug`', () => {
        test('Должен вернуть слаг', () => {
            expect(filters.slug('Должен вернуть слаг'))
                .toBe('dolzhen-vernut-slag');
        });
        test('Должен заменить лишние символы', () => {
            expect(filters.slug('    Должен _ вернуть слаг?!" /*  '))
                .toBe('dolzhen-vernut-slag');
        });
    });

    describe('Фильтр `url`', () => {
        test('Должен найти URL в меню для переданного `id`', () => {
            expect(filters.url(context.menu, ['g']))
                .toBe('id-g.html');
        });
        test('Должен найти URL в меню для переданного `path`', () => {
            expect(filters.url(context.menu, ['a/e/g']))
                .toBe('id-g.html');
        });
        test('Должен найти URL в меню для переданного `slug`', () => {
            expect(filters.url(context.menu, ['id-g']))
                .toBe('id-g.html');
        });
        test('Должен найти URL в меню для переданного `slug`, сохранив хеш', () => {
            expect(filters.url(context.menu, ['id-g#top']))
                .toBe('id-g.html#top');
        });
    });

    describe('Фильтр `extra`', () => {
        test('Должен конвертировать хеш объект в строку HTML атрибутов', () => {
            expect(filters.extra({'id': 'id_name', 'name': 'name', 'value': 1}))
                .toBe('id="id_name" name="name" value="1"');
        });
        test('Должен сохранить порядок', () => {
            expect(filters.extra({'value': 1, 'name': 'name', 'id': 'id_name'}))
                .toBe('id="id_name" name="name" value="1"');
        });
    });

    describe('Фильтр `test`', () => {
        test('Должен искать по регулярке', () => {
            expect(filters.test('Q', ['[q]', 'i']))
                .toBeTruthy();
        });
    });

    describe('Фильтр `inGroup`', () => {
        test('Должен вернуть true если не указана группа', () => {
            expect(filters.inGroup({group: 'header'}, []))
                .toBeTruthy();
        });
        test('Должен вернуть true если у элемента меню группа совпала', () => {
            expect(filters.inGroup({group: 'header'}, ['header']))
                .toBeTruthy();
        });
        test('Должен вернуть true если у элемента меню есть указанная группа', () => {
            expect(filters.inGroup({group: 'header, footer'}, ['header']))
                .toBeTruthy();
        });
        test('Должен вернуть false если у элемента меню не указана группа', () => {
            expect(filters.inGroup({}, ['header']))
                .toBeFalsy();
        });
        test('Должен вернуть false если у элемента меню нет указанной группы', () => {
            expect(filters.inGroup({group: 'header, footer'}, ['submenu']))
                .toBeFalsy();
        });
        test('Должен вернуть false если у элемента меню не совпала группа', () => {
            expect(filters.inGroup({group: 'header'}, ['submenu']))
                .toBeFalsy();
        });
    });

    describe('Фильтр `jsClass`', () => {
        test('Должен вернуть имя класса для простого случая', () => {
            expect(filters.jsClass({jsClassName: 'js-popup'}, ['toggle']))
                .toBe(' js-popup-toggle');
        });
        test('Должен вернуть имя класса для сложного случая', () => {
            expect(filters.jsClass({jsClassName: 'js-popup-menu'}, ['toggle']))
                .toBe(' js-popup-toggle');
        });
        test('Должен вернуть имя исходного класса если не переданы параметры', () => {
            expect(filters.jsClass({jsClassName: 'js-popup-menu'}, []))
                .toBe(' js-popup-menu');
        });

        test('Должен вернуть имя класса для простого случая, когда указано несколько базовых классов', () => {
            expect(filters.jsClass({jsClassName: 'js-popup js-animation'}, ['toggle']))
                .toBe(' js-popup-toggle js-animation');
        });
        test('Должен вернуть имя класса для сложного случая, когда указано несколько базовых классов', () => {
            expect(filters.jsClass({jsClassName: 'js-popup-menu js-animation'}, ['toggle']))
                .toBe(' js-popup-toggle js-animation');
        });
        test('Должен вернуть имя исходного класса если не переданы параметры, когда указано несколько базовых классов', () => {
            expect(filters.jsClass({jsClassName: 'js-popup-menu js-animation'}, []))
                .toBe(' js-popup-menu js-animation');
        });

        test('Должен вернуть пустую строку, если нет базового класса', () => {
            expect(filters.jsClass({jsClassName: ''}, ['toggle']))
                .toBe('');
        });
        test('Должен вернуть базовый класс, если не передан параметр', () => {
            expect(filters.jsClass({jsClassName: 'js-popup'}, []))
                .toBe(' js-popup');
        });
    });

    describe('Фильтр `sortBy`', () => {
        test('Должен отсортировать массив по ключу `key`', () => {
            expect(filters.sortBy(context.sortable, ['key']))
                .toMatchObject([
                    {key: 0, value: 4},
                    {key: 1, value: 3},
                    {key: 2, value: 2},
                    {key: 3, value: 1},
                    {key: 4, value: 0}
                ]);
        });
        test('Должен отсортировать массив по ключу `value`', () => {
            expect(filters.sortBy(context.sortable, ['value']))
                .toMatchObject([
                    {key: 4, value: 0},
                    {key: 3, value: 1},
                    {key: 2, value: 2},
                    {key: 1, value: 3},
                    {key: 0, value: 4}
                ]);
        });
    });
});
