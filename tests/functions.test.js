const twigHelpers = require('../twig-helpers');

const context = require('./context.json');

const functions = twigHelpers.functions(context.params);

describe('Twig Функции:', () => {
    describe('Функция `core(path)`', () => {
        test('Должна правильно разрешать путь в папке `core`', () => {
            expect(functions.core('js/global.js'))
                .toBe('/static/core/js/global.js');
        });
    });

    describe('Функция `vendor(path)`', () => {
        test('Должна правильно разрешать путь в папке `vendor`', () => {
            expect(functions.vendor('jquery.min.js'))
                .toBe('/static/vendor/jquery.min.js');
        });
    });

    describe('Функция `media(path)`', () => {
        test('Должна правильно разрешать путь в папке `media`', () => {
            expect(functions.media('image.jpg'))
                .toBe('/static/core/markup-media/image.jpg');
        });
    });

    describe('Функция `getMenu`', () => {
        test('Должна возвращать меню для переданного `id`', () => {
            expect(functions.getMenu(context.menu, ['e']))
                .toMatchObject(context.target.children);
        });
        test('Должна возвращать меню для переданного `slug`', () => {
            expect(functions.getMenu(context.menu, ['id-e']))
                .toMatchObject(context.target.children);
        });
        test('Должна возвращать меню для переданного `path`', () => {
            expect(functions.getMenu(context.menu, ['a/e']))
                .toMatchObject(context.target.children);
        });
    });

    describe('Функция `getMenuItem`', () => {
        test('Должна возвращать пункт меню для переданного `id`', () => {
            expect(functions.getMenuItem(context.menu, ['e']))
                .toMatchObject(context.target);
        });
        test('Должна возвращать пункт меню для переданного `slug`', () => {
            expect(functions.getMenuItem(context.menu, ['id-e']))
                .toMatchObject(context.target);
        });
        test('Должна возвращать пункт меню для переданного `path`', () => {
            expect(functions.getMenuItem(context.menu, ['a/e']))
                .toMatchObject(context.target);
        });
    });

    describe('Функция `getPageTitle`', () => {
        test('Должна возвращать заголовок страницы из переданного объекта, если в нём есть `title`', () => {
            let page = {id: 'e', title: 'Заголовок страницы'};
            expect(functions.getPageTitle(page, context.menu))
                .toBe('Заголовок страницы');
        });
        test('Должна возвращать заголовок страницы из меню, если в переданном объекте есть `id`', () => {
            let page = {id: 'e'};
            expect(functions.getPageTitle(page, context.menu))
                .toBe('Заголовок страницы (e)');
        });
        test('Должна возвращать пустую строку если в переданном объекте нет `title` и `id`', () => {
            let page = {};
            expect(functions.getPageTitle(page, context.menu))
                .toBe('');
        });
    });

    describe('Функция `getRoot`', () => {
        test('Должна возвращать родительский объект первого уровня меню для объекта страницы с `path`', () => {
            let page = {path: 'a/e/f'};
            let target = functions.getMenuItem(context.menu, 'a');
            expect(functions.getRoot(page, context.menu))
                .toMatchObject(target);
        });
        test('Должна возвращать родительский объект второго уровня меню для объекта страницы с `path` и `rootLevel = 2`', () => {
            let page = {path: 'a/e/f'};
            let target = functions.getMenuItem(context.menu, 'e');
            expect(functions.getRoot(page, context.menu, 2))
                .toMatchObject(target);
        });
    });

    describe('Функция `getPage`', () => {
        test('Должна расшиорить объект из меню переданным объектом страницы с `id`', () => {
            let page = {id: 'e', layout: 'default'};
            let target = Object.assign({}, page, context.target);
            expect(functions.getPage(page, context.menu))
                .toMatchObject(target);
        });
    });

    describe('Функция `getParentFor`', () => {
        test('Должен найти родительский пункт меню для переданного `id`', () => {
            expect(functions.getParentFor(context.menu, 'g'))
                .toMatchObject(context.target);
        });
        test('Должен найти родительский пункт меню для переданного `path`', () => {
            expect(functions.getParentFor(context.menu, 'a/e/g'))
                .toMatchObject(context.target);
        });
        test('Должен найти родительский пункт меню для переданного `slug`', () => {
            expect(functions.getParentFor(context.menu, 'id-g'))
                .toMatchObject(context.target);
        });
    });

    describe('Функция `isActive`', () => {
        test('Должна возвращать true, если menuItem.id есть в page.path', () => {
            let page = {path: 'a/e/f'};
            expect(functions.isActive(context.target, page))
                .toBeTruthy();
        });
        test('Должна возвращать false, если menuItem.id нет в page.path', () => {
            let page = {path: 'a/b/d'};
            expect(functions.isActive(context.target, page))
                .toBeFalsy();
        });
    });

    describe('Функция `isCurrent`', () => {
        test('Должна возвращать true, если menuItem.id последняя в page.path', () => {
            let page = {path: 'a/e'};
            expect(functions.isCurrent(context.target, page))
                .toBeTruthy();
        });
        test('Должна возвращать false, если menuItem.id нет в page.path', () => {
            let page = {path: 'a/b'};
            expect(functions.isCurrent(context.target, page))
                .toBeFalsy();
        });
        test('Должна возвращать false, если menuItem.id не последняя page.path', () => {
            let page = {path: 'a/b/c'};
            let target = functions.getMenuItem(context.menu, 'b');
            expect(functions.isCurrent(target, page))
                .toBeFalsy();
        });
    });
});

